package com.unibit.nba_games.services;

import com.unibit.nba_games.models.NbaGameModel;
import com.unibit.nba_games.models.UpcomingGamesModel;
import com.unibit.nba_games.models.playersmodels.OuterPlayerLayer;
import com.unibit.nba_games.models.playersmodels.PlayerInfoModel;
import com.unibit.nba_games.repositories.contracts.NbaApiRepository;
import com.unibit.nba_games.services.contracts.NbaApiService;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class NbaApiServiceImpl implements NbaApiService {

    private final NbaApiRepository repository;

    public NbaApiServiceImpl(NbaApiRepository repository) {
        this.repository = repository;
    }

    @Cacheable(value = "players", key = "#playerName")
    public PlayerInfoModel getPlayerInfo(String playerName) {
        Optional<OuterPlayerLayer> result = repository.getPlayers(playerName);
        OuterPlayerLayer body = result.get();
        PlayerInfoModel playerInfoModel = null;
        if (body.getData() != null && body.getData().size() > 0) {
            playerInfoModel = body.getData().get(0);
        } else {
            throw new NullPointerException();
        }

        return playerInfoModel;
    }

    @Cacheable(value = "games")
    public List<NbaGameModel> getNextTenGames() {
       return updateNextTenGames();
    }

    @CachePut(value = "games")
    @Scheduled(cron = "${schedulers.at-midnight}")
    public List<NbaGameModel> updateNextTenGames() {
        Optional<UpcomingGamesModel> result = repository.getLatest12Games();

        List<NbaGameModel> games = new ArrayList<>();
        if (result.isPresent()) {
            games = Objects.requireNonNull(result.get()).getData();
        }

        games = removeStartedAndExtraGames(games);

        return games;
    }

    private List<NbaGameModel> removeStartedAndExtraGames(List<NbaGameModel> games) {
        List<NbaGameModel> result = new ArrayList<>(games);

        result.removeIf(game -> !game.getTime().isEmpty());

        while (result.size() > 10) {
            result.remove(result.size() - 1);
        }

        return result;
    }

}
