package com.unibit.nba_games.services.contracts;

import com.unibit.nba_games.models.NbaGameModel;
import com.unibit.nba_games.models.playersmodels.PlayerInfoModel;

import java.util.List;

public interface NbaApiService {
    List<NbaGameModel> getNextTenGames();

    PlayerInfoModel getPlayerInfo(String playerName);
}
