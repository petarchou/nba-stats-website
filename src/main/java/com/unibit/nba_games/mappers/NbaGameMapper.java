package com.unibit.nba_games.mappers;

import com.unibit.nba_games.models.NbaGameModel;
import com.unibit.nba_games.models.dtos.NbaGameDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface NbaGameMapper {

    @Mapping(source = "home_team.full_name", target = "home_team_name")
    @Mapping(source = "visitor_team.full_name", target = "visitor_team_name")
    @Mapping(source="date", dateFormat = "YYYY-MM-DD", target = "date")
    NbaGameDto toNbaGameDto(NbaGameModel model);
}
