package com.unibit.nba_games.mappers;

import com.unibit.nba_games.models.dtos.PlayerDto;
import com.unibit.nba_games.models.playersmodels.PlayerInfoModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface PlayerMapper {



    @Mapping(target = "full_name", expression = "java(p.getFirst_name()+\" \" + p.getLast_name())")
    @Mapping(source = "team.full_name", target="team_name")
    PlayerDto toPlayerDto(PlayerInfoModel p);

}
