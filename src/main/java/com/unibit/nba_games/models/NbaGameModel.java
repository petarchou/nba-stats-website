package com.unibit.nba_games.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class NbaGameModel {

    private long id;
    private LocalDate date;
    private TeamModel home_team;
    private int home_team_score;
    private int visitor_team_score;
    private int period;
    private String status;
    //time in current period - is empty when string when game not started/complete
    private String time;
    private TeamModel visitor_team;


    public long getId() {
        return id;
    }

    public NbaGameModel setId(long id) {
        this.id = id;
        return this;
    }

    public LocalDate getDate() {
        return date;
    }

    public NbaGameModel setDate(LocalDate date) {
        this.date = date;
        return this;
    }

    public TeamModel getHome_team() {
        return home_team;
    }

    public NbaGameModel setHome_team(TeamModel home_team) {
        this.home_team = home_team;
        return this;
    }

    public int getHome_team_score() {
        return home_team_score;
    }

    public NbaGameModel setHome_team_score(int home_team_score) {
        this.home_team_score = home_team_score;
        return this;
    }

    public int getVisitor_team_score() {
        return visitor_team_score;
    }

    public NbaGameModel setVisitor_team_score(int visitor_team_score) {
        this.visitor_team_score = visitor_team_score;
        return this;
    }

    public int getPeriod() {
        return period;
    }

    public NbaGameModel setPeriod(int period) {
        this.period = period;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public NbaGameModel setStatus(String status) {
        this.status = status;
        return this;
    }

    public String getTime() {
        return time;
    }

    public NbaGameModel setTime(String time) {
        this.time = time;
        return this;
    }

    public TeamModel getVisitor_team() {
        return visitor_team;
    }

    public NbaGameModel setVisitor_team(TeamModel visitor_team) {
        this.visitor_team = visitor_team;
        return this;
    }

}
