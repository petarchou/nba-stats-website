package com.unibit.nba_games.models.playersmodels;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.unibit.nba_games.models.TeamModel;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PlayerInfoModel {

    private long id;
    private String first_name;
    private String last_name;
    private int height_feet;
    private int height_inches;
    private String position;
    private TeamModel team;

    public long getId() {
        return id;
    }

    public PlayerInfoModel setId(long id) {
        this.id = id;
        return this;
    }

    public String getFirst_name() {
        return first_name;
    }

    public PlayerInfoModel setFirst_name(String first_name) {
        this.first_name = first_name;
        return this;
    }

    public String getLast_name() {
        return last_name;
    }

    public PlayerInfoModel setLast_name(String last_name) {
        this.last_name = last_name;
        return this;
    }

    public int getHeight_feet() {
        return height_feet;
    }

    public PlayerInfoModel setHeight_feet(int height_feet) {
        this.height_feet = height_feet;
        return this;
    }

    public int getHeight_inches() {
        return height_inches;
    }

    public PlayerInfoModel setHeight_inches(int height_inches) {
        this.height_inches = height_inches;
        return this;
    }

    public String getPosition() {
        return position;
    }

    public PlayerInfoModel setPosition(String position) {
        this.position = position;
        return this;
    }

    public TeamModel getTeam() {
        return team;
    }

    public PlayerInfoModel setTeam(TeamModel team) {
        this.team = team;
        return this;
    }
}
