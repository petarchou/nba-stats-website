package com.unibit.nba_games.models.playersmodels;

import java.util.List;

public class OuterPlayerLayer {

    List<PlayerInfoModel> data;


    public List<PlayerInfoModel> getData() {
        return data;
    }

    public OuterPlayerLayer setData(List<PlayerInfoModel> data) {
        this.data = data;
        return this;
    }
}
