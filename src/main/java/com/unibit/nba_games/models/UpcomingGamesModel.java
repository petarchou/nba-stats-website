package com.unibit.nba_games.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;


public class UpcomingGamesModel {

    private List<NbaGameModel> data;

    public List<NbaGameModel> getData() {
        return data;
    }

    public UpcomingGamesModel setData(List<NbaGameModel> data) {
        this.data = data;
        return this;
    }
}
