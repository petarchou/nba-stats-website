package com.unibit.nba_games.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TeamModel {

    private long id;
    private String abbreviation;
    private String full_name;

    public long getId() {
        return id;
    }

    public TeamModel setId(long id) {
        this.id = id;
        return this;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public TeamModel setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
        return this;
    }

    public String getFull_name() {
        return full_name;
    }

    public TeamModel setFull_name(String full_name) {
        this.full_name = full_name;
        return this;
    }
}
