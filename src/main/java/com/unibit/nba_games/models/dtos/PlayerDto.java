package com.unibit.nba_games.models.dtos;

import com.unibit.nba_games.models.TeamModel;

public class PlayerDto {

    private String full_name;
    private int height_feet;
    private int height_inches;
    private String position;
    private String team_name;


    public String getFull_name() {
        return full_name;
    }

    public PlayerDto setFull_name(String full_name) {
        this.full_name = full_name;
        return this;
    }

    public int getHeight_feet() {
        return height_feet;
    }

    public PlayerDto setHeight_feet(int height_feet) {
        this.height_feet = height_feet;
        return this;
    }

    public int getHeight_inches() {
        return height_inches;
    }

    public PlayerDto setHeight_inches(int height_inches) {
        this.height_inches = height_inches;
        return this;
    }

    public String getPosition() {
        return position;
    }

    public PlayerDto setPosition(String position) {
        this.position = position;
        return this;
    }

    public String getTeam_name() {
        return team_name;
    }

    public PlayerDto setTeam_name(String team_name) {
        this.team_name = team_name;
        return this;
    }
}
