package com.unibit.nba_games.models.dtos;

import com.unibit.nba_games.models.TeamModel;

import java.time.LocalDateTime;

public class NbaGameDto {

    private String date;
    private String status;
    private String home_team_name;
    private String visitor_team_name;
    private int home_team_score;
    private int visitor_team_score;
    private int period;
    //time in current period - is empty when string when game not started/complete
    private String time;


    public String getDate() {
        return date;
    }

    public NbaGameDto setDate(String date) {
        this.date = date;
        return this;
    }

    public String getHome_team_name() {
        return home_team_name;
    }

    public NbaGameDto setHome_team_name(String home_team_name) {
        this.home_team_name = home_team_name;
        return this;
    }

    public String getVisitor_team_name() {
        return visitor_team_name;
    }

    public NbaGameDto setVisitor_team_name(String visitor_team_name) {
        this.visitor_team_name = visitor_team_name;
        return this;
    }

    public int getHome_team_score() {
        return home_team_score;
    }

    public NbaGameDto setHome_team_score(int home_team_score) {
        this.home_team_score = home_team_score;
        return this;
    }

    public int getVisitor_team_score() {
        return visitor_team_score;
    }

    public NbaGameDto setVisitor_team_score(int visitor_team_score) {
        this.visitor_team_score = visitor_team_score;
        return this;
    }

    public int getPeriod() {
        return period;
    }

    public NbaGameDto setPeriod(int period) {
        this.period = period;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public NbaGameDto setStatus(String status) {
        this.status = status;
        return this;
    }

    public String getTime() {
        return time;
    }

    public NbaGameDto setTime(String time) {
        this.time = time;
        return this;
    }
}
