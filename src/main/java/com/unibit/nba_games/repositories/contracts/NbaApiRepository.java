package com.unibit.nba_games.repositories.contracts;

import com.unibit.nba_games.models.UpcomingGamesModel;
import com.unibit.nba_games.models.playersmodels.OuterPlayerLayer;

import java.util.Optional;

public interface NbaApiRepository {

    Optional<OuterPlayerLayer> getPlayers(String playerName);
    Optional<UpcomingGamesModel> getLatest12Games();

}
