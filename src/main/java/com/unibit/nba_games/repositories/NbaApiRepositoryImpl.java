package com.unibit.nba_games.repositories;

import com.unibit.nba_games.models.UpcomingGamesModel;
import com.unibit.nba_games.models.playersmodels.OuterPlayerLayer;
import com.unibit.nba_games.repositories.contracts.NbaApiRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

import static java.lang.String.format;

@Repository
public class NbaApiRepositoryImpl implements NbaApiRepository {

    public static final String UPCOMING_GAMES_URL = "https://www.balldontlie.io/api/v1/games?start_date=%s&per_page=12";
    public static final String PLAYER_URL = "https://www.balldontlie.io/api/v1/players?search=%s";
    private final RestTemplate restTemplate;


    public NbaApiRepositoryImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public Optional<OuterPlayerLayer> getPlayers(String playerName) {
        ResponseEntity<OuterPlayerLayer> result = restTemplate.getForEntity
                (format(PLAYER_URL, playerName), OuterPlayerLayer.class);

        if (result.getBody() != null && result.getBody().getData() != null) {
            return Optional.of(result.getBody());
        }
        return Optional.empty();
    }

    public Optional<UpcomingGamesModel> getLatest12Games() {
        String date = LocalDate.now().format(DateTimeFormatter.ISO_DATE);
        ResponseEntity<UpcomingGamesModel> result = restTemplate.getForEntity
                (format(UPCOMING_GAMES_URL, date), UpcomingGamesModel.class);

        if (result.getBody() != null && result.getBody().getData() != null) {
            return Optional.of(result.getBody());
        }
        return Optional.empty();
    }
}
