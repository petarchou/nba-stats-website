package com.unibit.nba_games.controllers;

import com.unibit.nba_games.mappers.NbaGameMapper;
import com.unibit.nba_games.mappers.PlayerMapper;
import com.unibit.nba_games.models.dtos.PlayerDto;
import com.unibit.nba_games.models.dtos.NbaGameDto;
import com.unibit.nba_games.services.contracts.NbaApiService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class NbaInfoRestController {

    private final NbaGameMapper gameMapper;

    private final NbaApiService nbaApiService;

    private final PlayerMapper playerMapper;

    public NbaInfoRestController(NbaGameMapper gameMapper, NbaApiService nbaApiService, PlayerMapper playerMapper) {
        this.gameMapper = gameMapper;
        this.nbaApiService = nbaApiService;
        this.playerMapper = playerMapper;
    }


    @GetMapping("/players")
    public ResponseEntity<PlayerDto> getPlayerInfo(@RequestParam("name") String name) {

        PlayerDto playerDto = playerMapper.toPlayerDto(nbaApiService.getPlayerInfo(name));
        return ResponseEntity.ok(playerDto);
    }


    @GetMapping("/games")
    public ResponseEntity<List<NbaGameDto>> getUpcomingGames() {
        List<NbaGameDto> games = nbaApiService.getNextTenGames()
                .stream()
                .map(gameMapper::toNbaGameDto)
                .collect(Collectors.toList());

        return ResponseEntity.ok(games);
    }

}
