package com.unibit.nba_games.controllers;

import com.unibit.nba_games.mappers.NbaGameMapper;
import com.unibit.nba_games.models.dtos.NbaGameDto;
import com.unibit.nba_games.services.contracts.NbaApiService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping
public class ViewController {

    private final NbaApiService nbaApiService;
    private final NbaGameMapper gameMapper;

    public ViewController(NbaApiService nbaApiService, NbaGameMapper gameMapper) {
        this.nbaApiService = nbaApiService;
        this.gameMapper = gameMapper;
    }


    @GetMapping
    public ModelAndView getHomePage() {
        ModelAndView modelAndView = new ModelAndView("index");


        return modelAndView;
    }

    @GetMapping("/players")
    public ModelAndView getBestPlayers() {
        ModelAndView modelAndView = new ModelAndView("players");


        return modelAndView;
    }

    @GetMapping("/games")
    public ModelAndView getNextGames() {
        ModelAndView modelAndView = new ModelAndView("games");

        List<NbaGameDto> games = nbaApiService.getNextTenGames()
                .stream()
                .map(gameMapper::toNbaGameDto)
                .collect(Collectors.toList());

        modelAndView.addObject("games",games);

        return modelAndView;
    }

}
