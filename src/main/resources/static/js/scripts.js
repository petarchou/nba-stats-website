const submitForm = () => {
    const inputField = document.getElementById('player-name');
    const playerName = inputField.value;
    const mutableState = document.getElementById('mutable-state');

    fetch('http://localhost:8080/api/players?name='+playerName)
        .then(response => {
            if(response.ok)
                return response.json();
            throw new Error('No player found.');
        })
        .then(player => {

            let node = document.createElement('td');
            let node2 = document.createElement('td');
            let node3 = document.createElement('td');
            let node4 = document.createElement('td');

            let name = document.createTextNode(player.full_name);
            let height = document.createTextNode(player.height_feet + ' feet ' + player.height_inches + ' inches');
            let position = document.createTextNode(player.position);
            let team = document.createTextNode(player.team_name);

            node.append(name);
            node2.append(height);
            node3.append(position);
            node4.append(team);

            while(mutableState.firstChild) {
                mutableState.removeChild(mutableState.lastChild);
            }

            mutableState.append(node,node2,node3,node4);
        })
        .catch(error => {
            console.log(error);
            while(mutableState.firstChild) {
                mutableState.removeChild(mutableState.lastChild);
            }
        })

    inputField.value = '';
}

const btn = document.querySelector('#search-player')
btn.addEventListener("click",submitForm);
